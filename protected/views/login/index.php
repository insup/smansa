<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SMANSA - Login</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body >

    <div class="container">
    <div class="container">
        <div class="row text-center " style="padding-top:200px;" >
            <!-- <div class="col-md-12" >
                <img src="assets/assets/img/login.png" width = "400px"/ ;">
            </div> -->
        </div>
         <div class="row ">
               
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                    <br/>
                    <div class="panel-heading"></div>                    
                            <div class="kotaklogin">
							<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'login-form',
								'enableClientValidation'=>true,
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
								),
							)); ?>
                                <form role="form"><center>                                    
                                    <br/>
                                     <div class="form-group input-group">
                                            Username
						<?php echo $form->textField($model,'username'); ?>
											
                                        </div>
                                                                              <div class="form-group input-group">
                                            Password&nbsp;
						<?php echo $form->passwordField($model,'password'); ?>
                                        </div> 									                                   
                                         <input type="submit" class="btn btn-primary form-control login-btn" value="Login" width = "200px">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/buatakun">Buat Akun</a> || <a href="<?php echo Yii::app()->request->baseUrl; ?>/buatakun/forget">Lupa Password</a>
                                     <br/>
                                     
                                     </center>

                                    </form>
                                     
									<?php $this->endWidget(); ?>
                            </div>
                            <div class="panel-foot"></div>
                            <br/>
                        </div>
                
                
        </div>
    </div>
</body>
</html>

