<?php
/* @var $this TampilNilaiAdminController */

$this->breadcrumbs=array(
	'Tampil Nilai Admin',
);
?>
<h1><center>Data Nilai Siswa Kelas X SMAN 1 UNGARAN</center></h1>
<br/>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>Nama Siswa</th>
			<th>Nilai IPA</th>
			<th>Nilai IPS</th>
			<th>Nilai Matematika</th>
			<th>Nilai Tes Masuk</th>
			<th>Jurusan</th>
			<th colspan="2">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($hasil as $key): ?>		
	<tr>
		<td><?php echo $key['nama'] ?></td>
		<td><?php echo AlatUmum::formatDecimal($key['ipa']) ?></td>
		<td><?php echo AlatUmum::formatDecimal($key['ips']) ?></td>
		<td><?php echo AlatUmum::formatDecimal($key['mat']) ?></td>
		<td><?php echo $key['tes'] ?></td>
		<td><?php echo $key['nama_jurusan'] ?></td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/tampilnilaiadmin/undelete/<?php echo $key['nisn'] ?>">Restore</a></td>
	</tbody>
	<?php endforeach ?>
</table>
Export ke <a href="<?php echo $this->createUrl("tampilnilai/toexcel/fileName/Data_Nilai_Siswa");?>">Excel</a>