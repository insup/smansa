<?php
/* @var $this HomeController */

$this->breadcrumbs=array(
	'Home',
);
?>
<font face="Verdana">
	<h1 class="page-head-line">Profil SMAN 1 Ungaran</h1>
	<p class="page-subhead-line">SMA Negeri 1 Ungaran berdiri ditandai dengan keluarnya SK Menteri Pendidikan dan Kebudayaan RI tanggal 17 Juli 1965 dengan Nomor 96/SK/B/65-66 tentang pendirian SMA Negeri Ungaran. Didirikan di kota Ungaran karena menjadi kota kawedanan dan calon ibu kota Kabupaten Semarang. Pada awal berdiri jumlah murid kelas 111 orang dan menempati gedung SRL ( Sekolah Rakyat Latihan ) sebanyak 4 kelas. Baru 1968 gedung diberikan kepada SMA dengan lokasi Jalan Diponegoro 185 Ungaran. Yang tepat bersebalahan dengan SD Sidomulyo. Pada perkembangan berikutnya, SD Sidomulyo dihibahkan kepada SMA sehingga area lebih luas. Dengan demikian kegiatan pendidikan dapat dilaksanakan dengan tertib dan lancar.</p>

	<p class="page-subhead-line">Pada tahun 1983 nama SMA Negeri Ungaran berubah menjadi SMA Negeri 1 Ungaran, seiring dengan berdirinya SMA Negeri 2 Ungaran di wilayah kecamatan Ungaran. Selanjutnya seiring dengan perubahan kurikulum 1984 menjadi kurikulum 1994 berganti nama menjadi SMU Negeri 1 Ungaran dan sekarang di era otonomi daerah berganti menjadi SMA Negeri 1 Ungaran di bawah Dinas Pendidikan Kab. Semarang.</p>

	<h4 class="heading4">Visi Sekolah</h4>

	<p class="page-subhead-line">“ Menjadi sekolah yang SERASI (Sehat, Rapi, Aman, Sejuk, Indah) dalam mewujudkan warga sekolah yang bertaqwa, berbudaya, unggul dalam prestasi, dan berwawasan Internasional”.</p>

	<h4 class="heading4">Misi Sekolah</h4>
	<ol>
		<li>Meningkatkan kedisiplinan warga sekolah agar berbudaya bersih dan tertib</li>
    	<li>Meningkatkan keimanan danketaqwaan terhadap Tuhan Yang Maha Esa melalui pendalaman dan pengamalan ajaran agama</li>
    	<li>Meningkatkan kecintaan terhadapbudaya dan seni daerah sehinggamenjadi salah satu sumber kearifan berperilaku dan bermasyarakat</li>
    	<li>Meningkatkan Kompetensi pendidik dan tenaga kependidikan agar berprestasi dan memiliki etos kerja yang tinggi</li>
    	<li>Meningkatkan prestasi peserta didik di bidang akademik melalui peningkatan kualitas proses dan hasil pembelajaran</li>
    	<li>Meningkatkan prestasi peserta didik di bidang nonakademik melalui efektivitas pembinaan ekstrakurikuler dan keikutsertaan dalam kejuaraan</li>
    	<li>Menumbuhkan wawasan internasional warga sekolah melalui penguasaan teknologi informasi dan kompetensi bahasa asing</li>
    </ol>

	<h4 class="heading4">Tujuan Sekolah</h4>
	<ol>
    	<li>Menciptakan sekolah yang SERASI (Sehat, Rapi, Aman, Sejuk, Indah)</li>
    	<li>Menyusun dan melaksanakan tatatertib dan segala ketentuan yang mengatur operasional warga sekolah</li>
    	<li>Menyediakan sarana dan prasarana pendidikan serta sarana ibadah yang memadai</li>
    	<li>Menyediakan wahana dalam peningkatan apresiasi budaya dan seni daerah</li>
    	<li>Melaksanakan proses belajar mengajar secara efektif dan efisien, berdasarkan semangat keunggulan lokal dan global</li>
    	<li>Meningkatkan kinerja pendidik dan tenaga kependidikan (kepala sekolah, guru, karyawan) untuk bersama-sama melaksanakan kegiatan yang inovatif sesuai dengan tugas pokok dan fungsi (TUPOKSI) masing-masing</li>
    	<li>Meningkatkan efektivitas kegiatan program ekstrakurikuler sesuai bakat dan minat peserta didik sebagai salah satu sarana pengembangan diri peserta didik</li>
    	<li>Mewujudkan peningkatan kualitas dan kuantitas tamatan yang melanjutkan ke perguruan tinggi</li>
    	<li>Meningkatkan wawasan pendidik, tenaga kependidikan, dan peserta didik yang dapat berkompetisi di tingkat lokal maupun global</li>
    </ol>
</font>