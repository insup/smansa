<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SMANSA UNGARAN</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="nav-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header tengahin">                
                <img width="80px" height="80px" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/img/logo.png" />
            </div>

            <div class="header-right">
                <font size="4px"><marquee>SPK Penjurusan Siswa SMAN 1 Ungaran</marquee></font>
            </div>
            
			
        </nav>
        <br/>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <?php
                                if((Yii::app()->user->isManager())||(Yii::app()->user->isAdmin())||(Yii::app()->user->isMember()))
                                     { ?>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                <?php echo Yii::app()->user->name ?>                                
                            </div>
                            <?php }?>
                        </div>

                    </li>
                    <?php
                      if((Yii::app()->user->isManager())||(Yii::app()->user->isAdmin())||(Yii::app()->user->isMember()))
                         { ?> 
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/home"><i class="fa fa-tag"></i>Home</a>
                    </li>
                    <?php }?>

                    <?php
                      if((Yii::app()->user->isMember()))
                         { ?> 
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/dataku/<?php Yii::app()->user->name ?>"><i class="fa fa-tag"></i>Dataku</a>
                    </li>
                    <?php }?>

                     <?php
                      if((Yii::app()->user->isMember()))
                         { ?> 
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/jurusanku/<?php Yii::app()->user->name ?>"><i class="fa fa-tag"></i>Jurusanku</a>
                    </li>
                    <?php }?>

                    <?php
                              if((Yii::app()->user->isAdmin()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputdatasiswa"><i class="fa fa-tag"></i>Input Data Siswa</a>
                    </li>
                    <?php }?>

                    <?php
                              if((Yii::app()->user->isManager()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputnilai"><i class="fa fa-tag"></i>Input Data Nilai Siswa</a>
                    </li>
                    <?php }?>

                    <?php
                              if((Yii::app()->user->isManager()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/tampilDataSiswa"><i class="fa fa-tag"></i>Tampilkan Data Siswa</a>
                    </li>
                    <?php }?>

                    <?php
                              if((Yii::app()->user->isAdmin()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/tampilDataSiswaAdmin"><i class="fa fa-tag"></i>Tampilkan Data Siswa</a>
                    </li>
                    <?php }?>

                    <?php
                              if((Yii::app()->user->isManager()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/tampilnilai"><i class="fa fa-tag"></i>Tampilkan Data Nilai Siswa</a>
                    </li>
                    <?php }?>
                    
                    <?php
                              if((Yii::app()->user->isAdmin()))
                                 { ?>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/tampilnilaiadmin"><i class="fa fa-tag"></i>Tampilkan Data Nilai Siswa</a>
                    </li>
                    <?php }?>

                    <?php
                      if((Yii::app()->user->isManager())||(Yii::app()->user->isAdmin())||(Yii::app()->user->isMember()))
                         { ?> 
                    <li>
                        <a  tabindex="-1" href="<?php echo Yii::app()->request->baseUrl ?>/login/logout"><i class="fa fa-lock"></i>Logout</a>
                    </li>
                    <?php }?>
                    
                    <div class="tengahin"><font color="white">CopyLeft &copy; By EFIn</font></div>
                </ul>
        </div>

        </nav>
		<div id="page-wrapper">
            <div id="page-inner">
			
			
		<?php echo $content; ?>
		
		
		</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    <!-- </div>
		<div id="footer-sec" class="tengahin">
        &copy; 2016 Tim ICT PPs | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
    </div> -->
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/custom.js"></script>
    


</body>
</html>