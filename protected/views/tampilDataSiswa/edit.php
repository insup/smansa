<?php
/* @var $this TampilDataSiswaController */

$this->breadcrumbs=array(
	'Tampil Data Siswa'=>array('/tampilDataSiswa'),
	'Edit',
);
?>

<?php foreach ($hasil as $key): ?>
<div class="panel panel-info">
    <div class="panel-heading">
        Edit Data <?php echo $key['nama'] ?>
    </div>
<div class="panel-body">
<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/tampildatasiswa/update">
    <table>
        <tr><div class="form-group">
            <td><label>Sekolah Asal</label></td>
            <td></td>                                           
            <td><input class="form-control" type="text" name="asal" value="<?php echo $key['asal'] ?>"></td>
            </div>
        </tr>  
        <tr><div class="form-group">
            <td><label>Tanggal Lahir</label></td>
            <td></td>                                           
            <td><input class="form-control" type="text" name="ttl" value="<?php echo $key['ttl'] ?>"></td>
            </div>
        </tr>     
    </table>
    <input class="form-control" type="hidden" name="nisn" value="<?php echo $key['nisn'] ?>">
<?php endforeach ?> 
<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>UPDATE</button>

