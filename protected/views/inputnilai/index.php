<?php
/* @var $this InputnilaiController */

$this->breadcrumbs=array(
	'Inputnilai',
);
?>
<div class="panel panel-info">
    <div class="panel-heading">
        Input Data Nilai Siswa
    </div>
<div class="panel-body">
<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/inputnilai/Insertdata">
<font face="Californian FB">
    <table>
		<tr><div class="form-group">
                <td width="25%"><label>Nama Siswa</label></td>
                <!-- <td width="25%"></td> -->
                <td width="50%"><select class="form-control" id="nisn" name="nisn">
                    <?php foreach ($hasil as $key): ?>
                        <option value="<?php echo $key['nisn']; ?>"><?php echo $key['nama'] ?></option>
                    <?php endforeach ?>
                    </select>
                </td>                                                                     
            </div>
        </tr>
    </table>
    <hr/>
    <div class="form-group"><label>Nilai Raport IPA</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa1"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa2"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa3"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa4"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa5"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa6"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <div class="form-group"><label>Nilai Raport IPS</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips1"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips2"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips3"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips4"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips5"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips6"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <div class="form-group"><label>Nilai Raport Matematika</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat1"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat2"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat3"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat4"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat5"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat6"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Nilai Tes Masuk</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="tes"></td>
            </div>
        </tr>
    </table>
    <hr/>
</font>
<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button>