<?php
/* @var $this TampilNilaiController */

$this->breadcrumbs=array(
	'Tampil Nilai',
);
?>
<?php foreach ($hasil as $key): ?>
<div class="panel panel-info">
    <div class="panel-heading">
        Edit Data Nilai <?php echo $key['nama'] ?>
    </div>
<div class="panel-body">
<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/tampilnilai/update">
<font face="Californian FB">
    <div class="form-group"><label>Nilai Raport IPA</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa1" value="<?php echo $key['ipa1'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa2" value="<?php echo $key['ipa2'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa3" value="<?php echo $key['ipa3'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa4" value="<?php echo $key['ipa4'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa5" value="<?php echo $key['ipa5'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ipa6" value="<?php echo $key['ipa6'] ?>"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <div class="form-group"><label>Nilai Raport IPS</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips1" value="<?php echo $key['ips1'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips2" value="<?php echo $key['ips2'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips3" value="<?php echo $key['ips3'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips4" value="<?php echo $key['ips4'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips5" value="<?php echo $key['ips5'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="ips6" value="<?php echo $key['ips6'] ?>"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <div class="form-group"><label>Nilai Raport Matematika</label></div>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 1</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat1" value="<?php echo $key['mat1'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 2</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat2" value="<?php echo $key['mat2'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 3</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat3" value="<?php echo $key['mat3'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 4</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat4" value="<?php echo $key['mat4'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 5</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat5" value="<?php echo $key['mat5'] ?>"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td width="30%"><label>Semester 6</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="mat6" value="<?php echo $key['mat6'] ?>"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <table>
        <tr><div class="form-group">
            <td width="30%"><label>Nilai Tes Masuk</label></td>
            <td width="25%"></td>                                           
            <td width="50%"><input class="form-control" type="text" name="tes" value="<?php echo $key['tes'] ?>"></td>
            </div>
        </tr>
    </table>
    <hr/>
    <input class="form-control" type="hidden" name="nisn" value="<?php echo $key['nisn'] ?>">
</font>
<?php endforeach ?>
<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>UPDATE</button>