<?php
/* @var $this ExportController */

$this->breadcrumbs=array(
	'Export',
);
?>
<h1><center>Data Siswa Kelas X SMAN 1 UNGARAN</center></h1>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>No.</th>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Asal Sekolah</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($siswa as $key): ?>		
	<tr>
		<td><?php echo $key->no_daftar ?></td>
		<td ><?php echo $key->nisn ?></td>
		<td><?php echo $key->nama ?></td>
		<td><?php echo $key->asal ?></td>
	</tr>
	</tbody>
	<?php endforeach ?>
	Export ke <a href="<?php echo Yii::app()->request->baseUrl; ?>/export/exportPdf">PDF</a>
</table>