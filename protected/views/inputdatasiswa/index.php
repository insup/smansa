<?php
/* @var $this InputdatasiswaController */

$this->breadcrumbs=array(
	'Inputdatasiswa',
);
?>
<div class="panel panel-info">
    <div class="panel-heading">
        Input Data Siswa
    </div>
<div class="panel-body">
<table>
	<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/inputdatasiswa/Insertdata">
		<tr><div class="form-group">
            <td><label>NISN</label></td>
			<td></td>											
            <td><input class="form-control" type="text" name="nisn"></td>
            </div>
        </tr>

        <tr><div class="form-group">
            <td><label>Nama</label></td>
			<td></td>											
            <td><input class="form-control" type="text" name="nama"></td>
            </div>
        </tr>

        <tr><div class="form-group">
            <td><label>Sekolah Asal</label></td>
			<td></td>											
            <td><input class="form-control" type="text" name="asal"></td>
            </div>
        </tr>       

        <tr><div class="form-group">
            <td><label>Tanggal Lahir</label></td>
            <td></td>                                           
            <td><input class="form-control" type="text" name="ttl" placeholder="yyyy-mm-dd"></td>
            </div>
        </tr>
</table>	
<button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button>
