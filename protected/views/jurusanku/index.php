<?php
/* @var $this JurusankuController */

$this->breadcrumbs=array(
	'Jurusanku',
);
?>
<div class="container">
            <br/>
            <table>
            	<?php foreach ($hasil as $key): ?>
            	<tr>
            		<td>Nama</td>
            		<td width="20%"></td>
            		<td><?php echo $key['nama'] ?></td>
            	</tr>

            	<tr>
            		<td>NISN</td>
            		<td width="20%"></td>
            		<td><?php echo $key['nisn'] ?></td>
            	</tr>

            	<tr>
            		<td>Nilai Rata-rata IPA</td>
            		<td width="20%"></td>
            		<td><?php echo AlatUmum::formatDecimal($key['ipa']) ?></td>
            	</tr>

            	<tr>
            		<td>Nilai Rata-rata IPS</td>
            		<td width="20%"></td>
            		<td><?php echo AlatUmum::formatDecimal($key['ips']) ?></td>
            	</tr>

            	<tr>
            		<td>Nilai Rata-rata Matematika</td>
            		<td width="20%"></td>
            		<td><?php echo AlatUmum::formatDecimal($key['mat']) ?></td>
            	</tr>

            	<tr>
            		<td>Nilai Tes Masuk</td>
            		<td width="20%"></td>
            		<td><?php echo $key['tes'] ?></td>
            	</tr>
            </table>
            <br/>
        <p class="page-subhead-line">Selamat, anda diterima di jurusan <font face="Showcard Gothic" size="10px"><?php echo $key['nama_jurusan'] ?>.</font></p>
	<?php endforeach ?>
</div>