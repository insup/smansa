<?php
/* @var $this TampilDataSiswaAdminController */

$this->breadcrumbs=array(
	'Tampil Data Siswa Admin',
);
?>
<h1><center>Data Siswa Kelas X SMAN 1 UNGARAN</center></h1>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>No.</th>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Asal Sekolah</th>
			<th>Tanggal Lahir</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php $a=0;?>
		<?php foreach ($hasil as $key): ?>
		<?php $a++; ?>		
	<tr>
		<td><?php echo $a ?></td>
		<td><?php echo $key['nisn'] ?></td>
		<td><?php echo $key['nama'] ?></td>
		<td><?php echo $key['asal'] ?></td>
		<td><?php echo $key['ttl'] ?></td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/tampildatasiswaadmin/undelete/<?php echo $key['nisn'] ?>">Restore</a></td>
	</tr>
	</tbody>
	<?php endforeach ?>	
</table>
Export ke <a href="<?php echo $this->createUrl("tampilDataSiswa/toexcel/fileName/Data_Siswa_SMANSA_Ungaran");?>">Excel</a>
