<?php

/**
 * This is the model class for table "nilai".
 *
 * The followings are the available columns in table 'nilai':
 * @property string $nisn
 * @property double $raport_ipa1
 * @property double $raport_ipa2
 * @property double $raport_ipa3
 * @property double $raport_ipa4
 * @property double $raport_ipa5
 * @property double $raport_ipa6
 * @property double $raport_ips1
 * @property double $raport_ips2
 * @property double $raport_ips3
 * @property double $raport_ips4
 * @property double $raport_ips5
 * @property double $raport_ips6
 * @property double $raport_mat1
 * @property double $raport_mat2
 * @property double $raport_mat3
 * @property double $raport_mat4
 * @property double $raport_mat5
 * @property double $raport_mat6
 * @property double $tes_masuk
 * @property integer $status
 */
class Nilai extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nilai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nisn, raport_ipa1, raport_ipa2, raport_ipa3, raport_ipa4, raport_ipa5, raport_ipa6, raport_ips1, raport_ips2, raport_ips3, raport_ips4, raport_ips5, raport_ips6, raport_mat1, raport_mat2, raport_mat3, raport_mat4, raport_mat5, raport_mat6, tes_masuk', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('raport_ipa1, raport_ipa2, raport_ipa3, raport_ipa4, raport_ipa5, raport_ipa6, raport_ips1, raport_ips2, raport_ips3, raport_ips4, raport_ips5, raport_ips6, raport_mat1, raport_mat2, raport_mat3, raport_mat4, raport_mat5, raport_mat6, tes_masuk', 'numerical'),
			array('nisn', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nisn, raport_ipa1, raport_ipa2, raport_ipa3, raport_ipa4, raport_ipa5, raport_ipa6, raport_ips1, raport_ips2, raport_ips3, raport_ips4, raport_ips5, raport_ips6, raport_mat1, raport_mat2, raport_mat3, raport_mat4, raport_mat5, raport_mat6, tes_masuk, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nisn' => 'Nisn',
			'raport_ipa1' => 'Raport Ipa1',
			'raport_ipa2' => 'Raport Ipa2',
			'raport_ipa3' => 'Raport Ipa3',
			'raport_ipa4' => 'Raport Ipa4',
			'raport_ipa5' => 'Raport Ipa5',
			'raport_ipa6' => 'Raport Ipa6',
			'raport_ips1' => 'Raport Ips1',
			'raport_ips2' => 'Raport Ips2',
			'raport_ips3' => 'Raport Ips3',
			'raport_ips4' => 'Raport Ips4',
			'raport_ips5' => 'Raport Ips5',
			'raport_ips6' => 'Raport Ips6',
			'raport_mat1' => 'Raport Mat1',
			'raport_mat2' => 'Raport Mat2',
			'raport_mat3' => 'Raport Mat3',
			'raport_mat4' => 'Raport Mat4',
			'raport_mat5' => 'Raport Mat5',
			'raport_mat6' => 'Raport Mat6',
			'tes_masuk' => 'Tes Masuk',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('raport_ipa1',$this->raport_ipa1);
		$criteria->compare('raport_ipa2',$this->raport_ipa2);
		$criteria->compare('raport_ipa3',$this->raport_ipa3);
		$criteria->compare('raport_ipa4',$this->raport_ipa4);
		$criteria->compare('raport_ipa5',$this->raport_ipa5);
		$criteria->compare('raport_ipa6',$this->raport_ipa6);
		$criteria->compare('raport_ips1',$this->raport_ips1);
		$criteria->compare('raport_ips2',$this->raport_ips2);
		$criteria->compare('raport_ips3',$this->raport_ips3);
		$criteria->compare('raport_ips4',$this->raport_ips4);
		$criteria->compare('raport_ips5',$this->raport_ips5);
		$criteria->compare('raport_ips6',$this->raport_ips6);
		$criteria->compare('raport_mat1',$this->raport_mat1);
		$criteria->compare('raport_mat2',$this->raport_mat2);
		$criteria->compare('raport_mat3',$this->raport_mat3);
		$criteria->compare('raport_mat4',$this->raport_mat4);
		$criteria->compare('raport_mat5',$this->raport_mat5);
		$criteria->compare('raport_mat6',$this->raport_mat6);
		$criteria->compare('tes_masuk',$this->tes_masuk);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Nilai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
