<?php

/**
 * This is the model class for table "derajat".
 *
 * The followings are the available columns in table 'derajat':
 * @property string $nisn
 * @property double $ipa_rendah
 * @property double $ips_rendah
 * @property double $mat_rendah
 * @property double $tes_rendah
 * @property double $ipa_sedang
 * @property double $ips_sedang
 * @property double $mat_sedang
 * @property double $tes_sedang
 * @property double $ipa_tinggi
 * @property double $ips_tinggi
 * @property double $mat_tinggi
 * @property double $tes_tinggi
 * @property integer $id_jurusan
 */
class Derajat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'derajat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nisn, ipa_rendah, ips_rendah, mat_rendah, tes_rendah, ipa_sedang, ips_sedang, mat_sedang, tes_sedang, ipa_tinggi, ips_tinggi, mat_tinggi, tes_tinggi, id_jurusan', 'required'),
			array('id_jurusan', 'numerical', 'integerOnly'=>true),
			array('ipa_rendah, ips_rendah, mat_rendah, tes_rendah, ipa_sedang, ips_sedang, mat_sedang, tes_sedang, ipa_tinggi, ips_tinggi, mat_tinggi, tes_tinggi', 'numerical'),
			array('nisn', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nisn, ipa_rendah, ips_rendah, mat_rendah, tes_rendah, ipa_sedang, ips_sedang, mat_sedang, tes_sedang, ipa_tinggi, ips_tinggi, mat_tinggi, tes_tinggi, id_jurusan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nisn' => 'Nisn',
			'ipa_rendah' => 'Ipa Rendah',
			'ips_rendah' => 'Ips Rendah',
			'mat_rendah' => 'Mat Rendah',
			'tes_rendah' => 'Tes Rendah',
			'ipa_sedang' => 'Ipa Sedang',
			'ips_sedang' => 'Ips Sedang',
			'mat_sedang' => 'Mat Sedang',
			'tes_sedang' => 'Tes Sedang',
			'ipa_tinggi' => 'Ipa Tinggi',
			'ips_tinggi' => 'Ips Tinggi',
			'mat_tinggi' => 'Mat Tinggi',
			'tes_tinggi' => 'Tes Tinggi',
			'id_jurusan' => 'Id Jurusan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nisn',$this->nisn,true);
		$criteria->compare('ipa_rendah',$this->ipa_rendah);
		$criteria->compare('ips_rendah',$this->ips_rendah);
		$criteria->compare('mat_rendah',$this->mat_rendah);
		$criteria->compare('tes_rendah',$this->tes_rendah);
		$criteria->compare('ipa_sedang',$this->ipa_sedang);
		$criteria->compare('ips_sedang',$this->ips_sedang);
		$criteria->compare('mat_sedang',$this->mat_sedang);
		$criteria->compare('tes_sedang',$this->tes_sedang);
		$criteria->compare('ipa_tinggi',$this->ipa_tinggi);
		$criteria->compare('ips_tinggi',$this->ips_tinggi);
		$criteria->compare('mat_tinggi',$this->mat_tinggi);
		$criteria->compare('tes_tinggi',$this->tes_tinggi);
		$criteria->compare('id_jurusan',$this->id_jurusan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Derajat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
