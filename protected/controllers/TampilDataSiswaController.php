<?php

class TampilDataSiswaController extends YiinigoController
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql = "select nisn as nisn, nama as nama, asal as asal, tgl_lahir as ttl from siswa where status=1";

	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionToexcel($fileName)
	{
		$this->createExcel($fileName);
		$sql = "select nisn as nisn, nama as nama, asal as asal, tgl_lahir as ttl from siswa where status=1";

	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();

		$dataExcel=$this->renderPartial('report', array('hasil'=>$hasil,
			));
		echo $dataExcel;
	}

	public function actionDelete($id)
	{
		$sql = "UPDATE penjurusan.siswa SET siswa.status=0 WHERE siswa.nisn = '$id'";
		$sql1 = "UPDATE penjurusan.nilai SET nilai.status=0 WHERE nilai.nisn = '$id'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->execute();
		$command1 = $connection->createCommand($sql1);
		$hasil1 = $command1->execute();
		$this->redirect('/sman1/tampildatasiswa/');
	}

	public function actionEdit($id)
	{
		$sql = "select nisn as nisn, nama, asal, tgl_lahir as ttl from siswa where nisn='$id'";

		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('edit', array('hasil'=>$hasil));	
	}

	public function actionUpdate()
	{
		if ($_POST)
		{
			$nisn = $_POST['nisn'];
			$asal = $_POST['asal'];
			$ttl = $_POST['ttl'];

			$sql = "UPDATE penjurusan.siswa SET siswa.asal='$asal', siswa.tgl_lahir='$ttl' WHERE siswa.nisn = '$nisn'";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);
			$hasil = $command->execute();
			$this->redirect('/sman1/tampildatasiswa/');
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}