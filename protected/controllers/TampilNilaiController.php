<?php

class TampilNilaiController extends YiinigoController
{
	public $layout = "main";

	public function actionIndex()
	{
		$sql = "select nilai.nisn as nisn, siswa.nama as nama, ((nilai.raport_ipa1+nilai.raport_ipa2+nilai.raport_ipa3+nilai.raport_ipa4+nilai.raport_ipa5+nilai.raport_ipa6)/6) as ipa,((nilai.raport_ips1+nilai.raport_ips2+nilai.raport_ips3+nilai.raport_ips4+nilai.raport_ips5+nilai.raport_ips6)/6) as ips, ((nilai.raport_mat1+nilai.raport_mat2+nilai.raport_mat3+nilai.raport_mat4+nilai.raport_mat5+nilai.raport_mat6)/6) as mat, tes_masuk as tes, derajat.id_jurusan, jurusan.nama_jurusan from nilai, siswa, derajat, jurusan where nilai.nisn = siswa.nisn && nilai.nisn = derajat.nisn && derajat.id_jurusan = jurusan.id_jurusan && nilai.status=1";

	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionToexcel($fileName)
	{
		$this->createExcel($fileName);
		$sql = "select nilai.nisn as nisn, siswa.nama as nama, ((nilai.raport_ipa1+nilai.raport_ipa2+nilai.raport_ipa3+nilai.raport_ipa4+nilai.raport_ipa5+nilai.raport_ipa6)/6) as ipa,((nilai.raport_ips1+nilai.raport_ips2+nilai.raport_ips3+nilai.raport_ips4+nilai.raport_ips5+nilai.raport_ips6)/6) as ips, ((nilai.raport_mat1+nilai.raport_mat2+nilai.raport_mat3+nilai.raport_mat4+nilai.raport_mat5+nilai.raport_mat6)/6) as mat, tes_masuk as tes, derajat.id_jurusan, jurusan.nama_jurusan from nilai, siswa, derajat, jurusan where nilai.nisn = siswa.nisn && nilai.nisn = derajat.nisn && derajat.id_jurusan = jurusan.id_jurusan";

	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();

		$dataExcel=$this->renderPartial('report', array('hasil'=>$hasil,));
		echo $dataExcel;
	}

	public function actionHitung()
	{
		$sql1 = "TRUNCATE derajat";
		//$sql = "select * from perumahan";
		
		$sql = "select nilai.nisn, siswa.nama as nama, ((nilai.raport_ipa1+nilai.raport_ipa2+nilai.raport_ipa3+nilai.raport_ipa4+nilai.raport_ipa5+nilai.raport_ipa6)/6) as ipa,((nilai.raport_ips1+nilai.raport_ips2+nilai.raport_ips3+nilai.raport_ips4+nilai.raport_ips5+nilai.raport_ips6)/6) as ips, ((nilai.raport_mat1+nilai.raport_mat2+nilai.raport_mat3+nilai.raport_mat4+nilai.raport_mat5+nilai.raport_mat6)/6) as mat, tes_masuk as tes from nilai, siswa where nilai.nisn = siswa.nisn";
	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$command1 = $connection->createCommand($sql1);
		$hasil1 = $command1->execute();
		$hasil = $command->queryAll();

		foreach($hasil as $row) :
			$nisn = $row['nisn'];

			$ipa_rendah = $this->trapmf($row['ipa'], 0, 0, 50, 65);
			$ipa_sedang = $this->tripmf($row['ipa'], 60, 70, 80);
			$ipa_tinggi = $this->trapmf($row['ipa'], 75, 85, 100, 100);

			$ips_rendah = $this->trapmf($row['ips'], 0, 0, 50, 65);
			$ips_sedang = $this->tripmf($row['ips'], 63, 73, 83);
			$ips_tinggi = $this->trapmf($row['ips'], 78, 88, 100, 100);

			$mat_rendah = $this->trapmf($row['mat'], 0, 0, 50, 65);
			$mat_sedang = $this->tripmf($row['mat'], 60, 70, 80);
			$mat_tinggi = $this->trapmf($row['mat'], 75, 85, 100, 100);

			$tes_rendah = $this->trapmf($row['tes'], 0, 0, 150, 200);
			$tes_sedang = $this->tripmf($row['tes'], 175, 250, 350);
			$tes_tinggi = $this->trapmf($row['tes'], 320, 400, 480, 480);

			if ($ipa_tinggi!=0 && $mat_tinggi!=0 && $tes_tinggi!=0)
			{
				$id = 1;
			}
			elseif ($ipa_tinggi!=0 && $mat_sedang!=0 && $tes_tinggi!=0) {
				$id = 1;
			}
			elseif ($ipa_sedang!=0 && $mat_sedang!=0 && $tes_tinggi!=0) {
				$id = 1;
			}
			elseif ($ips_tinggi!=0 && $tes_tinggi!=0) {
				$id=2;
			}
			elseif ($ips_sedang!=0 && $tes_tinggi!=0) {
				$id=2;
			}
			elseif ($ips_sedang!=0 && $tes_sedang!=0) {
				$id=2;
			}
			else {
				$id =2;
			}

			$derajat = new Derajat;
				$derajat->nisn = $nisn;

				$derajat->ipa_rendah = $ipa_rendah;
				$derajat->ipa_sedang = $ipa_sedang;
				$derajat->ipa_tinggi = $ipa_tinggi;

				$derajat->ips_rendah = $ips_rendah;
				$derajat->ips_sedang = $ips_sedang;
				$derajat->ips_tinggi = $ips_tinggi;

				$derajat->mat_rendah = $mat_rendah;
				$derajat->mat_sedang = $mat_sedang;
				$derajat->mat_tinggi = $mat_tinggi;

				$derajat->tes_rendah = $tes_rendah;
				$derajat->tes_sedang = $tes_sedang;
				$derajat->tes_tinggi = $tes_tinggi;				

				$derajat->id_jurusan = $id;

				$derajat->save();
		endforeach;
		$this->redirect('/sman1/tampilnilai');
	}

	public function tripmf($nilai, $a, $b, $c)
	{
		$hasil = 0;
        if(($nilai<=$a)||($nilai>=$c)){$hasil = 0;}
        else if (($nilai>=$a) && ($nilai<=$b)){$hasil=($nilai-$a)/($b-$a);}
        else if (($nilai>=$b) && ($nilai<=$c)){$hasil=($c-$nilai)/($c-$b);}
        return $hasil;
	}

	public function trapmf($nilai, $a, $b, $c, $d)
	{
		$hasil = 0;
        if(($nilai<=$a)||($nilai>=$d)){$hasil = 0;}
        else if (($nilai>=$a) && ($nilai<=$b)){$hasil=($nilai-$a)/($b-$a);}
        else if (($nilai>=$b) && ($nilai<=$c)){$hasil=1;}
        else if (($nilai>=$c) && ($nilai<=$d)){$hasil=($d-$nilai)/($d-$c);}
        return $hasil;
	}

	public function actionEdit($id)
	{
		$sql = "select nilai.nisn as nisn, siswa.nama as nama, nilai.raport_ipa1 as ipa1, nilai.raport_ipa2 as ipa2, nilai.raport_ipa3 as ipa3, nilai.raport_ipa4 as ipa4, nilai.raport_ipa5 as ipa5, nilai.raport_ipa6 as ipa6, nilai.raport_ips1 as ips1, nilai.raport_ips2 as ips2, nilai.raport_ips3 as ips3, nilai.raport_ips4 as ips4, nilai.raport_ips5 as ips5, nilai.raport_ips6 as ips6, nilai.raport_mat1 as mat1, nilai.raport_mat2 as mat2, nilai.raport_mat3 as mat3, nilai.raport_mat4 as mat4, nilai.raport_mat5 as mat5, nilai.raport_mat6 as mat6, nilai.tes_masuk as tes from siswa, nilai where nilai.nisn=siswa.nisn && nilai.nisn = '$id'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('edit', array('hasil'=>$hasil));
	}

	public function actionUpdate()
	{
		if($_POST)
		{
			$nisn = $_POST['nisn'];

			$ipa1 = $_POST['ipa1'];
			$ipa2 = $_POST['ipa2'];
			$ipa3 = $_POST['ipa3'];
			$ipa4 = $_POST['ipa4'];
			$ipa5 = $_POST['ipa5'];
			$ipa6 = $_POST['ipa6'];

			$ips1 = $_POST['ips1'];
			$ips2 = $_POST['ips2'];
			$ips3 = $_POST['ips3'];
			$ips4 = $_POST['ips4'];
			$ips5 = $_POST['ips5'];
			$ips6 = $_POST['ips6'];

			$mat1 = $_POST['mat1'];
			$mat2 = $_POST['mat2'];
			$mat3 = $_POST['mat3'];
			$mat4 = $_POST['mat4'];
			$mat5 = $_POST['mat5'];
			$mat6 = $_POST['mat6'];

			$tes = $_POST['tes'];

			$sql = "UPDATE penjurusan.nilai SET nilai.raport_ipa1 = '$ipa1', nilai.raport_ipa2 = '$ipa2', nilai.raport_ipa3 = '$ipa3', nilai.raport_ipa4 = '$ipa4', nilai.raport_ipa5 = '$ipa5', nilai.raport_ipa6 = '$ipa6', nilai.raport_ips1 = '$ips1', nilai.raport_ips2 = '$ips2', nilai.raport_ips3 = '$ips3', nilai.raport_ips4 = '$ips4', nilai.raport_ips5 = '$ips5', nilai.raport_ips6 = '$ips6', nilai.raport_mat1 = '$mat1', nilai.raport_mat2 = '$mat2', nilai.raport_mat3 = '$mat3', nilai.raport_mat4 = '$mat4', nilai.raport_mat5 = '$mat5', nilai.raport_mat6 = '$mat6', nilai.tes_masuk = '$tes' WHERE nilai.nisn='$nisn'";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);
			$hasil = $command->execute();
			$this->redirect('/sman1/tampilnilai/');
		}
	}

	public function actionDelete($id)
	{
		$sql = "UPDATE penjurusan.nilai SET nilai.status=0 WHERE nilai.nisn = '$id'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->execute();
		$this->redirect('/sman1/tampilnilai/');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}