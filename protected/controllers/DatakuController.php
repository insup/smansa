<?php

class DatakuController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$nama = Yii::app()->user->name;
		$sql = "select user.nama, siswa.nisn as nisn, siswa.nama as nama, siswa.asal as asal, siswa.tgl_lahir as ttl from user, siswa where user.nama=siswa.nama && user.username = '$nama'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}