<?php

class InputdatasiswaController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionInsertdata()
	{
		if($_POST){
		
				$Siswa = new Siswa;

				echo $Siswa->nisn = $_POST['nisn'];
				echo $Siswa->nama = $_POST['nama'];
				echo $Siswa->asal = $_POST['asal'];
				echo $Siswa->tgl_lahir = $_POST['ttl'];

				if($Siswa->validate()){
					$Siswa->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/sman1/TampilDataSiswa');
				} else {
					$error = $Siswa->errors;
					print_r($error);
					// Yii::app()->user->setFlash($error);
					// $this->redirect(array('/sman1/inputdatasiswa'));
				}
			
		}
		else $this->actionIndex();
	}


	// public function filters()
	// {
	// 	return array(
	// 		'accessControl',
	// 		);
	// }

	// public function accessRules()
	// {
	// 	return array(
	// 		array('allow',
	// 			'actions'=>array('admin','index', 'inputdatasiswa'),
	// 			'expression'=>'$user->isAdmin()'
	// 			),
	// 		array('deny',
	// 			'users'=>array('*'),
	// 			),
	// 		);
	// }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}