<?php

class JurusankuController extends Controller
{
	public function actionIndex()
	{
		$nama = Yii::app()->user->name;
		$sql= "select nilai.nisn as nisn, user.nama as nama, ((nilai.raport_ipa1+nilai.raport_ipa2+nilai.raport_ipa3+nilai.raport_ipa4+nilai.raport_ipa5+nilai.raport_ipa6)/6) as ipa,((nilai.raport_ips1+nilai.raport_ips2+nilai.raport_ips3+nilai.raport_ips4+nilai.raport_ips5+nilai.raport_ips6)/6) as ips, ((nilai.raport_mat1+nilai.raport_mat2+nilai.raport_mat3+nilai.raport_mat4+nilai.raport_mat5+nilai.raport_mat6)/6) as mat, tes_masuk as tes, derajat.id_jurusan, jurusan.nama_jurusan as nama_jurusan from nilai, siswa, derajat, jurusan, user where nilai.nisn = siswa.nisn && nilai.nisn = derajat.nisn && derajat.id_jurusan = jurusan.id_jurusan && siswa.nama=user.nama && user.username LIKE '$nama'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}