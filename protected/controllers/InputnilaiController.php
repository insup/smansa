<?php

class InputnilaiController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "select * from siswa";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionInsertdata()
	{
		if($_POST){
		
				$nilai = new Nilai;

				 echo $nilai->nisn = $_POST['nisn'];
				 echo "<br>";
				 echo $nilai->raport_ipa1 = $_POST['ipa1'];
				 echo "<br>";
				 echo $nilai->raport_ipa2 = $_POST['ipa2'];
				 echo "<br>";
				 echo $nilai->raport_ipa3 = $_POST['ipa3'];
				 echo "<br>";
				 echo $nilai->raport_ipa4 = $_POST['ipa4'];
				 echo "<br>";
				 echo $nilai->raport_ipa5 = $_POST['ipa5'];
				 echo "<br>";
				 echo $nilai->raport_ipa6 = $_POST['ipa6'];
				 echo "<br>";
				 echo $nilai->raport_ips1 = $_POST['ips1'];
				 echo "<br>";
				 echo $nilai->raport_ips2 = $_POST['ips2'];
				 echo "<br>";
				 echo $nilai->raport_ips3 = $_POST['ips3'];echo "<br>";
				 echo $nilai->raport_ips4 = $_POST['ips4'];
				 echo "<br>";
				 echo $nilai->raport_ips5 = $_POST['ips5'];
				 echo "<br>";
				 echo $nilai->raport_ips6 = $_POST['ips6'];
				 echo "<br>";
				 echo $nilai->raport_mat1 = $_POST['mat1'];
				 echo "<br>";
				 echo $nilai->raport_mat2 = $_POST['mat2'];
				 echo "<br>";
				 echo $nilai->raport_mat3 = $_POST['mat3'];
				 echo "<br>";
				 echo $nilai->raport_mat4 = $_POST['mat4'];
				 echo "<br>";
				 echo $nilai->raport_mat5 = $_POST['mat5'];
				 echo "<br>";
				 echo $nilai->raport_mat6 = $_POST['mat6'];
				 echo "<br>";
				 echo $nilai->tes_masuk = $_POST['tes'];

				if($nilai->validate()){
					$nilai->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/sman1/home');
				} else {
					$error = $nilai->errors;
					print_r($error);
					// Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					$this->redirect(array('/errPage/errDB'));
				}
			
		}
		else $this->actionIndex();
	}

	// public function filters()
	// {
	// 	return array(
	// 		'accessControl',
	// 		);
	// }

	// public function accessRules()
	// {
	// 	return array(
	// 		array('allow',
	// 			'actions'=>array('manager','index', 'inputnilai'),
	// 			'expression'=>'$user->isManager()'
	// 			),
	// 		array('deny',
	// 			'users'=>array('*'),
	// 			),
	// 		);
	// }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}