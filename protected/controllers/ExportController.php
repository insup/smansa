<?php

class ExportController extends YiinigoController
{
	public $layout = "main";
	public function actionIndex()
	{
		$siswa = Siswa::model()->findAll();
		$this->render('index',array('siswa'=>$siswa,
			));
	}

	public function actionToexcel($fileName)
	{
		$this->createExcel($fileName);
		$siswa = Siswa::model()->findAll();

		$dataExcel=$this->renderPartial('report', array('siswa'=>$siswa,
			));
		echo $dataExcel;
	}

	 public static function actionExportPdf()
    {
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("InSup");
        $pdf->SetTitle("TCPDF Example 002");
        $pdf->SetSubject("TCPDF Tutorial");
        $pdf->SetKeywords("TCPDF, PDF, example, test, guide");
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        //$pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont("times", "BI", 20);
        $pdf->Cell(0,10,"Example 002",1,1,'C');
        $pdf->Output("example_002.pdf", "I");
    }
  


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}