<?php

class BuatakunController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "select * from siswa";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionCreate()
	{
		if($_POST){
		
				$user = new User;

				echo $user->nama = $_POST['nama'];
				echo "<br/>";
				echo $user->level = 0;
				echo "<br/>";
				echo $user->status = 1;
				echo "<br/>";
				echo $user->recoverable = 0;
				echo "<br/>";				
				echo $user->username = $_POST['username'];
				echo "<br/>";
				echo $user->password = md5($_POST['pass']);
				echo "<br/>";

				if($user->validate()){
					$user->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/sman1/');
				} else {
					$error = $user->errors;
					print_r($error);
					// Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					// $this->redirect(array('/errPage/errDB'));
				}			
		}
		else $this->actionIndex();
	}

	public function actionForget()
	{
		$this->render('forget');
	}

	public function actionPulihkan()
	{
		if($_POST)
		{
			$nama = $_POST['username'];
			$password =md5($_POST['pass']);
			$ttl = $_POST['ttl'];

			$sql = "select user.nama, siswa.tgl_lahir as t_lahir from penjurusan.user, penjurusan.siswa where user.nama = siswa.nama && user.username = '$nama'";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);
			$hasil = $command->queryAll();
			foreach($hasil as $row) :
				$tgl = $row['t_lahir'];
				if ($ttl==$tgl) {
				$this->restore($nama, $password);
				}
				else
				{
					$this->redirect('/sman1/buatakun/forget');
				}
			endforeach;
		}
	}


	public function restore($a, $b)
	{
		$sql = "UPDATE penjurusan.user SET user.password = '$b' WHERE user.username='$a'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->execute();
		$this->redirect('/sman1/');
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}