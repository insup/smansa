<?php

class TampilDataSiswaAdminController extends Controller
{
	public function actionIndex()
	{
		$sql = "select no_daftar as no, nisn as nisn, nama as nama, asal as asal, tgl_lahir as ttl from siswa where status=0";

	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->queryAll();
		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionUndelete($id)
	{
		$sql = "UPDATE penjurusan.siswa SET siswa.status=1 where siswa.nisn = '$id'";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$hasil = $command->execute();
		$this->redirect('/sman1/tampildatasiswaadmin/');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}