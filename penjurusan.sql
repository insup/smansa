-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2016 at 05:56 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `penjurusan`
--

-- --------------------------------------------------------

--
-- Table structure for table `derajat`
--

CREATE TABLE IF NOT EXISTS `derajat` (
  `nisn` varchar(20) NOT NULL,
  `ipa_rendah` double NOT NULL,
  `ips_rendah` double NOT NULL,
  `mat_rendah` double NOT NULL,
  `tes_rendah` double NOT NULL,
  `ipa_sedang` double NOT NULL,
  `ips_sedang` double NOT NULL,
  `mat_sedang` double NOT NULL,
  `tes_sedang` double NOT NULL,
  `ipa_tinggi` double NOT NULL,
  `ips_tinggi` double NOT NULL,
  `mat_tinggi` double NOT NULL,
  `tes_tinggi` double NOT NULL,
  `id_jurusan` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `derajat`
--

INSERT INTO `derajat` (`nisn`, `ipa_rendah`, `ips_rendah`, `mat_rendah`, `tes_rendah`, `ipa_sedang`, `ips_sedang`, `mat_sedang`, `tes_sedang`, `ipa_tinggi`, `ips_tinggi`, `mat_tinggi`, `tes_tinggi`, `id_jurusan`) VALUES
('302090018', 0, 0, 0, 0, 0.48333333333333, 0.76666666666667, 0, 0.2, 0.016666666666667, 0, 0.95, 0.125, 1),
('302090027', 0, 0, 0, 0, 0.93333333333333, 0.75, 0, 0, 0, 0, 0.75, 0.875, 2),
('302090036', 0, 0, 0, 0, 0, 0.85, 0, 0.54, 0.73333333333333, 0, 0.8, 0, 2),
('302090045', 0, 0, 0, 0, 0.4, 0.75, 0, 0, 0.1, 0, 0.68333333333333, 0.625, 1),
('302090054', 0, 0, 0, 0, 0.96666666666667, 0.91666666666667, 0.28333333333333, 0.96, 0, 0, 0.21666666666667, 0, 2),
('302090063', 0, 0, 0, 0, 0.68333333333333, 0.86666666666667, 0.083333333333333, 0.34, 0, 0, 0.41666666666667, 0, 2),
('302090072', 0, 0, 0, 0, 0, 0.33333333333333, 0, 0, 0.58333333333333, 0.16666666666667, 1, 1, 1),
('302090089', 0, 0, 0, 0, 0, 0.73333333333333, 0, 0.7, 0.5, 0, 0.93333333333333, 0, 2),
('302090098', 0, 0, 0, 0, 0.85, 0.71666666666667, 0.033333333333333, 0, 0, 0, 0.46666666666667, 0.425, 1),
('302090107', 0, 0, 0, 0, 0.86666666666667, 0.15, 0.016666666666667, 0.89333333333333, 0, 0.35, 0.48333333333333, 0, 2),
('302090116', 0, 0, 0, 0, 0, 0.38333333333333, 0, 0.74, 0.78333333333333, 0.11666666666667, 0.95, 0, 2),
('302090125', 0, 0, 0, 0, 0, 0, 0.016666666666667, 0, 1, 1, 0.48333333333333, 1, 1),
('302090134', 0, 0, 0, 0, 0.91666666666667, 0.98333333333333, 0, 0, 0, 0, 0.51666666666667, 0.8, 2),
('302090143', 0, 0, 0, 0, 0.91666666666667, 0.81666666666667, 0.15, 0.64, 0, 0, 0.35, 0, 2),
('302090152', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0.53333333333333, 1, 1, 1),
('302090169', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0.66666666666667, 0.88333333333333, 1, 1),
('302090178', 0, 0, 0, 0.68, 0.7, 0.83333333333333, 0, 0, 0, 0, 1, 0, 2),
('302090187', 0, 0, 0, 0, 0.91666666666667, 0.9, 0.2, 0.22, 0, 0, 0.3, 0.1, 1),
('302090196', 0, 0, 0, 0, 0.53333333333333, 0.81666666666667, 0, 0.28, 0, 0, 1, 0.025, 2),
('302090205', 0, 0, 0, 0, 0.93333333333333, 0.81666666666667, 0, 0.68, 0, 0, 1, 0, 2),
('302090214', 0, 0, 0, 0, 0.58333333333333, 0.16666666666667, 0, 0, 0, 0.33333333333333, 1, 1, 2),
('302090223', 0, 0, 0, 0, 0.68333333333333, 0.68333333333333, 0.2, 0.76, 0, 0, 0.3, 0, 2),
('302090258', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0.88333333333333, 1, 1, 1),
('302090277', 0, 0, 0, 1, 0, 0.73333333333333, 0, 0, 1, 0, 1, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `id_jurusan` int(20) NOT NULL,
  `nama_jurusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'IPA'),
(2, 'IPS');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `nisn` varchar(20) NOT NULL,
  `raport_ipa1` double NOT NULL,
  `raport_ipa2` double NOT NULL,
  `raport_ipa3` double NOT NULL,
  `raport_ipa4` double NOT NULL,
  `raport_ipa5` double NOT NULL,
  `raport_ipa6` double NOT NULL,
  `raport_ips1` double NOT NULL,
  `raport_ips2` double NOT NULL,
  `raport_ips3` double NOT NULL,
  `raport_ips4` double NOT NULL,
  `raport_ips5` double NOT NULL,
  `raport_ips6` double NOT NULL,
  `raport_mat1` double NOT NULL,
  `raport_mat2` double NOT NULL,
  `raport_mat3` double NOT NULL,
  `raport_mat4` double NOT NULL,
  `raport_mat5` double NOT NULL,
  `raport_mat6` double NOT NULL,
  `tes_masuk` double NOT NULL,
  UNIQUE KEY `nisn` (`nisn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nisn`, `raport_ipa1`, `raport_ipa2`, `raport_ipa3`, `raport_ipa4`, `raport_ipa5`, `raport_ipa6`, `raport_ips1`, `raport_ips2`, `raport_ips3`, `raport_ips4`, `raport_ips5`, `raport_ips6`, `raport_mat1`, `raport_mat2`, `raport_mat3`, `raport_mat4`, `raport_mat5`, `raport_mat6`, `tes_masuk`) VALUES
('302090018', 78, 84, 80, 62, 78, 69, 58, 76, 65, 89, 79, 85, 86, 79, 77, 75, 94, 96, 330),
('302090027', 58, 83, 68, 66, 78, 71, 43, 76, 56, 87, 78, 83, 97, 76, 85, 70, 88, 79, 390),
('302090036', 84, 84, 84, 82, 79, 81, 43, 77, 56, 87, 81, 85, 88, 86, 72, 77, 87, 88, 296),
('302090045', 76, 84, 79, 62, 84, 71, 38, 82, 55, 86, 79, 83, 78, 85, 72, 72, 100, 84, 370),
('302090054', 60, 85, 70, 56, 84, 67, 45, 83, 60, 85, 78, 82, 74, 70, 85, 70, 93, 71, 254),
('302090063', 72, 84, 77, 58, 81, 67, 45, 77, 58, 87, 79, 84, 87, 94, 77, 72, 74, 71, 316),
('302090072', 88, 85, 87, 70, 81, 74, 75, 77, 76, 88, 78, 84, 92, 98, 99, 80, 77, 78, 470),
('302090089', 80, 81, 81, 78, 81, 79, 38, 79, 54, 87, 80, 84, 95, 88, 81, 75, 88, 79, 280),
('302090098', 66, 82, 73, 60, 80, 68, 58, 80, 67, 87, 79, 84, 97, 68, 70, 73, 92, 78, 354),
('302090107', 64, 80, 71, 64, 79, 70, 78, 82, 80, 88, 77, 84, 71, 75, 68, 76, 99, 90, 242),
('302090116', 88, 84, 87, 76, 83, 79, 70, 80, 74, 86, 81, 84, 79, 98, 96, 81, 80, 73, 276),
('302090125', 92, 94, 93, 84, 89, 86, 100, 97, 99, 93, 87, 91, 71, 99, 76, 92, 73, 68, 450),
('302090134', 64, 85, 72, 54, 84, 66, 45, 82, 60, 87, 81, 84, 76, 69, 92, 71, 83, 90, 384),
('302090143', 56, 84, 67, 66, 80, 72, 40, 79, 56, 87, 80, 85, 85, 85, 69, 70, 75, 87, 286),
('302090152', 86, 89, 87, 82, 85, 83, 78, 86, 81, 87, 83, 85, 84, 98, 84, 84, 87, 87, 402),
('302090169', 94, 91, 93, 74, 91, 81, 83, 82, 82, 89, 85, 87, 86, 95, 76, 86, 91, 69, 404),
('302090178', 72, 85, 77, 56, 82, 66, 40, 79, 56, 88, 80, 85, 92, 81, 85, 71, 96, 94, 166),
('302090187', 50, 87, 65, 64, 79, 70, 43, 79, 57, 87, 81, 85, 76, 78, 86, 69, 80, 79, 328),
('302090196', 72, 84, 77, 64, 80, 71, 43, 79, 57, 87, 78, 83, 94, 80, 99, 72, 79, 90, 322),
('302090205', 60, 84, 70, 62, 79, 69, 43, 79, 57, 87, 78, 83, 79, 97, 83, 70, 95, 98, 226),
('302090214', 64, 87, 73, 68, 80, 73, 73, 82, 76, 89, 82, 86, 98, 84, 98, 77, 98, 97, 418),
('302090223', 72, 82, 76, 60, 81, 68, 58, 79, 66, 87, 82, 85, 77, 73, 94, 74, 76, 74, 232),
('302090232', 84, 89, 86, 72, 85, 77, 63, 80, 70, 89, 81, 86, 82, 100, 91, 80, 86, 90, 328),
('302090258', 89, 86, 90, 84, 86, 88, 80, 87, 88, 87, 90, 89, 89, 89, 89, 88, 90, 89, 450),
('302090277', 80, 89, 88, 89, 87, 90, 76, 75, 74, 78, 77, 74, 88, 89, 90, 89, 87, 86, 88);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `no_daftar` int(11) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `asal` varchar(50) NOT NULL,
  PRIMARY KEY (`no_daftar`),
  UNIQUE KEY `nisn` (`nisn`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`no_daftar`, `nisn`, `nama`, `asal`) VALUES
(1, '302090018', 'AHMAD KHOIRUL UMAM', 'MTs. Hasyimiyah Kalisidi'),
(2, '302090027', 'AHMAD NAUFAL', 'MTs. Hasyimiyah Kalisidi'),
(3, '302090036', 'ANGGA DWI YULIYANTO', 'SMPN 3 UNGARAN'),
(4, '302090045', 'ARIF SURYAWAN', 'SMPN 5 AMBARAWA'),
(5, '302090054', 'BAYU FAUZAN PATRIA', 'SMPN 11 SEMARANG'),
(6, '302090063', 'CANDRA ILMADAYANA', 'MTs. Mujahidin Mluweh'),
(7, '302090072', 'DEVINA PUTRI PRADANA', 'SMPN 4 UNGARAN'),
(8, '302090089', 'DONI KURNIAWAN', 'SMPN 2 BERGAS'),
(9, '302090098', 'EFNI SRI WAHYUNI', 'MTs. NU Ungaran'),
(10, '302090107', 'EMI LAILATUL FAJRIYAH', 'MTs. NU Ungaran'),
(11, '302090116', 'IBNU PURNAMA', 'SMP Miftahul Ulum Bergas'),
(12, '302090125', 'IIN SUPRIYATI', 'MTs. DIPONEGORO Mendiro'),
(13, '302090134', 'IMAM BUDI KRISTANTO', 'SMPN 5 BERGAS'),
(14, '302090143', 'IQBAL ALIB ARDIAN', 'SMP Modern Selamat Kendal'),
(15, '302090152', 'KHOIRUN NAFIAH', 'MTs. Hasyimiyah Kalisidi'),
(16, '302090169', 'LINDA NUR INDAHSARI', 'SMPN 5 Ungaran'),
(17, '302090178', 'LUQMAN HAKIM HISBULLOH', 'MTs. At-Thosari Kalrejo'),
(18, '302090187', 'MISBIANTORO', 'MTs. Ma''arif Nyatnyono'),
(19, '302090196', 'MUHAMAD ROFIUDIN', 'SMPN 3 Sumowono'),
(20, '302090205', 'MUHAMAD TEGA FAJARI', 'MTs. At-Thosari Kalrejo'),
(21, '302090214', 'MUHAMMAD ARIFIN', 'SMPN 2 Ungaran'),
(22, '302090223', 'MUHAMMAD HAIDAR MUJIBUDDIN', 'MTs. NU Ungaran'),
(23, '302090233', 'MUHAMMAD IBNU KHAFIDH', 'SMPN 3 Ungaran'),
(24, '302090249', 'MUNIROTUL MISBAHAH', 'MTs. MUJAHIDIN MLUWEH'),
(25, '302090258', 'MUSTA''INUL KARIMAH', 'SMPN 24 SEMARANG'),
(26, '302090277', 'Muhammad Ridwan', 'SMPN 6 Semarang');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `recoverable` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `level`, `status`, `recoverable`, `username`, `password`) VALUES
(1, 'admin', 2, 1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'manager', 1, 1, 0, 'manager', '1d0258c2440a8d19e716292b231e3190'),
(3, 'member', 0, 1, 0, 'member', 'aa08769cdcb26674c6706093503ff0a3'),
(4, 'iin sup', 0, 1, 0, 'insup', 'a87f96c26bd66dd366a25207e3f0f1d5'),
(5, 'fathoni khoirudin', 0, 1, 0, 'fathon', 'a529cb350bc6dfadb5148b201a8ca7a5'),
(6, 'nbjgbvj', 1, 1, 0, 'vtyhyj', 'a925576942e94b2ef57a066101b48876'),
(8, 'AHMAD KHOIRUL UMAM', 0, 1, 0, 'umama', 'dd82443e075f97089eb76d00d6043f43'),
(9, 'MISBIANTORO', 0, 1, 0, 'terong', 'a6cb4a13cc82db181a8e5f9e4c22b78d');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
